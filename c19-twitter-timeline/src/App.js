import React from 'react';
import logo from './logo.svg';
import './App.css';

import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

import { LoremIpsum, Avatar, name, surname, username } from 'react-lorem-ipsum';

import Linkify from 'react-linkify';

import casual from 'casual-browserify';

import results from './data/results.json';

const styleTimeline = {
    background: '#282C34', //'#eee', 
    //borderTop: "4px solid #fff"
}

const styleContentGenerate = () => {
    return ({
    background: 'rgb(33, 150, 243)',
    color: '#fff',
    borderTopWidth: '5px',
    borderTopColor: 'red',
    padding: '1em'
    });
}

const styleArrow = {
    borderRight: '7px solid  rgb(33, 150, 243)'
}

const styleIcon = {
    width: '80px',
    height: '80px',
    background: 'rgb(33, 150, 243)',
    color: '#fff',
    borderRadius: '50%'
}

const generateElements = (n) => {
    var results = [];
    for (var i = 0; i < n; i++) {
        var dateMock = casual.date('MMM DD, YYYY')
        results.push(
        <VerticalTimelineElement
            className="vert-01"
            date={dateMock}
            contentStyle={styleContentGenerate()}
            iconStyle={styleIcon}
            iconClassName="vertical-timeline-element-icon"
            icon={<Avatar style={styleIcon} />}
            >

            <h3 className="vertical-timeline-element-title">
                {name()} {surname()} (@{username()})
            </h3>
            <h4 className="vertical-timeline-element-subtitle">
                {casual.city}, {casual.state_abbr}
            </h4>
            <p style={{textTransform: 'initial', fontWeight: '200'}}>
                {casual.words(Math.floor(Math.random() * 50) + 6)}
            </p>
            <h5 style={{fontWeight: '400'}}>
                {dateMock}
            </h5>
        </VerticalTimelineElement>
        );
    }
    return results;
}

const generateElementsFromJson = (data) => {
    return (data.map( (key, value) => {
        return (
            <VerticalTimelineElement
                className="vert-01"
                date={data[value]['date'] + ' ' + data[value]['time']}
                contentStyle={styleContentGenerate()}
                iconStyle={styleIcon}
                iconClassName="vertical-timeline-element-icon"
                icon={<Avatar style={styleIcon} />}
                >

                <h3 className="vertical-timeline-element-title">
                    {data[value]['name']} (@{data[value]['username']})
                </h3>
                <h4 className="vertical-timeline-element-subtitle">
                    {casual.city}, {casual.state_abbr}
                </h4>
                <p style={{textTransform: 'initial', fontWeight: '200'}}>
                    <Linkify>{data[value]['tweet']}</Linkify>
                </p>
                <h5 style={{fontWeight: '400'}}>
                    Likes: {data[value]['likes_count']}, Retweets: {data[value]['retweets_count']}
                </h5>
            </VerticalTimelineElement>
        );
    }));
}

function App() {
  return (
    <div className="App">


        <div style={styleTimeline}>
            <VerticalTimeline>
                {generateElementsFromJson(results['data'])}
                {console.log(results['data'])}
            </VerticalTimeline>
        </div>
    </div>
  );
}

export default App;
