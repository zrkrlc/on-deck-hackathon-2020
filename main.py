import twint
import os
from pathlib import Path

PATH_TO_DATA = "./c19-twitter-timeline/src/data/"

def main(filename="results.json"):
    c = twint.Config()
    c.Search = "#COVID19"
    c.Limit = 40
    c.Store_json = True
    c.Store_object = True
    c.Output = filename
    c.Min_likes = 4
    c.Translate = True
    c.TranslateDest = "en"
    
    twint.run.Search(c)
    
    # Fix JSON output
    for i in range(5):
        print("\n")
        
    with open(filename, "r", encoding="utf-8") as f:
        s = f.readlines()
    with open(filename, "w", encoding="utf-8") as f:
        for i in range(len(s) - 1):
            s[i] = s[i] + ","
        s.insert(0, '{ "data": [\n')
        s.append("\n]}")
        f.writelines(s)
    
    os.replace("./" + filename, PATH_TO_DATA + filename)

if __name__ == "__main__":
    main()